#ifndef _MAIN_H
#define _MAIN_H
 
#ifdef __cplusplus
        extern "C" {
#endif
        void sayHello ();
	void setup(void);
	void loop(void);
#ifdef __cplusplus
        }
#endif
 
#endif
