public class HelloJNICpp {
   static {
      System.loadLibrary("hello"); // hello.dll (Windows) or libhello.so (Unixes)
   }
 
   public native void sayHello();
 
   public static void main(String[] args) {
      new HelloJNICpp().sayHello();
   }
}
